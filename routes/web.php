<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();


Route::get('/', 'PageController@welcome');

Route::get('/cart', 'PageController@cart');

Route::post('/search', "PageController@search");

Route::group(["middleware" => "auth"], function () {
	//for logged in users
	Route::get('/checkout', 'PageController@checkout');
	Route::get("/approval", "HomeController@approval")->name("approval");

	Route::middleware(["approved"])->group(function(){
		//for logged in and approved user
		Route::get('/home', 'HomeController@index')->name('home');
	});
	// Route::get("/confirmation/{id}", "PageController@confirmation");

	// Route::get('/products', 'ProductController@index')->middleware("auth");
	Route::group(["middleware" => ["isAdmin", "auth"]], function() 
	{
		Route::get("/users", "UserController@users");
		Route::get("/products", "ProductController@index");
		Route::delete("/products/{id}", "ProductController@destroy");
		Route::get("restore/{id}", "ProductController@restore");

		Route::get("products/{id}/edit", "ProductController@edit");
		Route::put("/products/{id}", "ProductController@update");
		Route::get("/products/create", "ProductController@create");

		Route::get("users/{id}/approve", "UserController@approve")->name("approve.user");
	});
});

// Route::get('/products', 'ProductController@index')->middleware("auth");
// Route::group(["middleware" => ["isAdmin", "auth"]], function() 
// {
// 	Route::get("/products", "ProductController@index");
// 	Route::delete("/products/{id}", "ProductController@destroy");
// 	Route::get("restore/{id}", "ProductController@restore");

// 	Route::get("products/{id}/edit", "ProductController@edit");
// 	Route::put("/products/{id}", "ProductController@update");
// 	Route::get("/products/create", "ProductController@create");
// });

Route::post("addToCart/{id}", "CartController@addToCart");
Route::put("/updateQuantity/{id}", "CartController@updateQuantity");
Route::delete("/destroyCartProduct/{id}", "CartController@destroyCartProduct");
Route::post("/confirmation/{id}", "PageController@confirmation")->middleware("auth");
