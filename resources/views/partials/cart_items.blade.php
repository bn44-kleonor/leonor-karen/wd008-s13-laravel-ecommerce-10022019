 This is cart item page
  	@if(Session::has('cart'))
		  		<!-- PRODUCTS -->
		  		@foreach($cart_products as $product)
			    <tr>
			      <th scope="row">
			      	<button 
			      	type="button" 
			      	class="btn btn-flat text-secondary btn-show-delete-modal" 
			      	data-id="{{ $product->id }}" 
			      	data-name="{{ $product->name }}"
			      	data-toggle="modal"
			      	data-target="#delete_cart_product_modal">
			      		X
			      	</button>
			      </th>
			      <td>{{ $product->name }}</td>
			      <td>&#36; {{ $product->price }}</td>
			      <td>
			      	<!-- UPDATE QUANTITY -->
			      	<form method="POST" class="update-quantity-form" id="update-quantity-form-{{ $product->id}}">
			      		@csrf
			      		{{ method_field("PUT") }}
				      	<div class="form-group">
				      		<input type="number" 
				      		class="form-control update-quantity" 
				      		min="1" value="{{ $product->quantity }}" 
				      		data-id="{{ $product->id }}" 
				      		data-name="{{ $product->name }}">
				      	</div>
				    </form>
			      </td>
			      <td>&#36; {{ $product->subtotal }}</td>
			      <td>
			      	<a href="#" class="btn btn-success">View</a>
			      </td>
			    </tr>
		  		@endforeach
		  	@endif
			<!-- TOTAL -->
				<tr>
					<td colspan="4" class="text-right">
						<h4>Total</h4>
					</td>
					<td colspan="4" class="text-right">
						<!-- variable from PageController.cart -->
						<h4>&#36; {{ $total }}</h4>
					</td>
				</tr>