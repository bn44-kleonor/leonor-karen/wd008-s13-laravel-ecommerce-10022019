<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->role == "admin")
        {
            return view("admin.index");
        }
            return view('home');
    }

    public function approval()
    {
        //dd(!Auth::user()->approved_at); //null
        if(Auth::user()->approved_at)
        {
            return view("home");
        }
        return view("approval");
    }
}